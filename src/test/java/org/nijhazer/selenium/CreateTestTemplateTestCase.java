package org.nijhazer.selenium;

import org.junit.Test;
import org.nijhazer.pagemodel.rms2.*;

public class CreateTestTemplateTestCase extends SpringBaseTestCase {
    private String host = "http://us11967.control4.com";

    @Test
    public void shouldUploadFile() throws Exception {
        driver.get(host);

        RMSLogin login = new RMSLogin(driver, wait);
        RMSMainNav mainNav = new RMSMainNav(driver, wait);
        RMSManageTestSuites manageTestSuites = new RMSManageTestSuites(driver, wait);
        RMSManageTestTemplates manageTestTemplates = new RMSManageTestTemplates(driver, wait);
        RMSCreateTestTemplate createTestTemplate = new RMSCreateTestTemplate(driver, wait);

        login.login("Administrator", "control4");

        mainNav.manageTestSuites();
        manageTestSuites.manageTestTemplates();
        manageTestTemplates.createTestTemplate();

        createTestTemplate.uploadFile("/Users/michaelwilson/Desktop/test_123.zip");
        //createTestTemplate.save();

        Thread.sleep(10000);
    }
}
