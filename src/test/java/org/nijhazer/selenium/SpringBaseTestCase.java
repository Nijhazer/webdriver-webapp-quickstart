package org.nijhazer.selenium;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={
    "classpath:spring.xml"
})
public class SpringBaseTestCase {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    protected RemoteWebDriver driver;
    protected WebDriverWait wait;

    @Test
    public void shouldNotDoAnything() throws Exception {

    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Inject
    public void setDriver(RemoteWebDriver driver) {
        this.driver = driver;
    }

    @Inject
    public void setWait(WebDriverWait wait) {
        this.wait = wait;
    }
}
