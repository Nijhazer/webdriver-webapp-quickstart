package org.nijhazer.pagemodel.rms2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class RMSCreateTestTemplate extends RMSPage {
    @FindBy(id="file_1")
    private WebElement fileControl;

    @FindBy(css=".c4-file-control > button")
    private WebElement browseButton;

    @FindBy(xpath = "//button[contains(., 'Save')]")
    private WebElement saveButton;

    public RMSCreateTestTemplate(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public WebElement getFileControl() {
        return fileControl;
    }

    public void setFileControl(WebElement fileControl) {
        this.fileControl = fileControl;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(WebElement saveButton) {
        this.saveButton = saveButton;
    }

    public WebElement getBrowseButton() {
        return browseButton;
    }

    public void setBrowseButton(WebElement browseButton) {
        this.browseButton = browseButton;
    }

    public void uploadFile(String filePath) {
        this.wait.until(ExpectedConditions.visibilityOf(getBrowseButton()));

        LocalFileDetector detector = new LocalFileDetector();

        File f = detector.getLocalFile(filePath);

        fileControl.sendKeys(f.getAbsolutePath());
    }
}
