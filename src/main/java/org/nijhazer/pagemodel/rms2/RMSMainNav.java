package org.nijhazer.pagemodel.rms2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMSMainNav extends RMSPage {
    @FindBy(xpath = "//a[@class='dropdown-toggle' and contains(., 'Administration')]")
    private WebElement administrationLink;

    public RMSMainNav(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public WebElement getAdministrationLink() {
        return administrationLink;
    }

    public void setAdministrationLink(WebElement administrationLink) {
        this.administrationLink = administrationLink;
    }

    public WebElement getManageTestSuitesLink() {
        return getAdministrationLink().findElement(By.xpath("//a[contains(., 'Test Suites')]"));
    }

    public void manageTestSuites() {
        this.wait.until(ExpectedConditions.visibilityOf(getAdministrationLink()));
        getAdministrationLink().click();
        getManageTestSuitesLink().click();
    }
}
