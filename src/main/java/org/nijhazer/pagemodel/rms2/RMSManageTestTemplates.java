package org.nijhazer.pagemodel.rms2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMSManageTestTemplates extends RMSPage {
    @FindBy(xpath = "//a[contains(., 'Create Test Template')]")
    private WebElement createTestTemplateLink;

    public RMSManageTestTemplates(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public WebElement getCreateTestTemplateLink() {
        return createTestTemplateLink;
    }

    public void setCreateTestTemplateLink(WebElement createTestTemplateLink) {
        this.createTestTemplateLink = createTestTemplateLink;
    }

    public void createTestTemplate() {
        this.wait.until(ExpectedConditions.visibilityOf(getCreateTestTemplateLink()));
        getCreateTestTemplateLink().click();
    }
}
