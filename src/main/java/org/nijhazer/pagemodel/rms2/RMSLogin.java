package org.nijhazer.pagemodel.rms2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMSLogin extends RMSPage {
    @FindBy(id="username")
    public WebElement username;

    @FindBy(id="password")
    public WebElement password;

    @FindBy(css="#c4-login-container button")
    public WebElement submitButton;

    public RMSLogin(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public WebElement getUsername() {
        return username;
    }

    public void setUsername(WebElement username) {
        this.username = username;
    }

    public WebElement getPassword() {
        return password;
    }

    public void setPassword(WebElement password) {
        this.password = password;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public void setSubmitButton(WebElement submitButton) {
        this.submitButton = submitButton;
    }

    public void login(String username, String password) {
        getUsername().sendKeys(username);
        getPassword().sendKeys(password);
        getSubmitButton().click();
    }
}
