package org.nijhazer.pagemodel.rms2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMSManageTestSuites extends RMSPage {
    @FindBy(xpath = "//a[contains(., 'Manage Test Templates')]")
    private WebElement manageTestTemplatesLink;

    public RMSManageTestSuites(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public WebElement getManageTestTemplatesLink() {
        return manageTestTemplatesLink;
    }

    public void setManageTestTemplatesLink(WebElement manageTestTemplatesLink) {
        this.manageTestTemplatesLink = manageTestTemplatesLink;
    }

    public void manageTestTemplates() {
        this.wait.until(ExpectedConditions.visibilityOf(getManageTestTemplatesLink()));
        getManageTestTemplatesLink().click();
    }
}
