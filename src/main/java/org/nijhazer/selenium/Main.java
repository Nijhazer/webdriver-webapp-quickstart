package org.nijhazer.selenium;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        context.registerShutdownHook();

        RemoteWebDriver driver = (RemoteWebDriver) context.getBean("driver");

        /**
         * If it were necessary to run Selenium inside of the main executable,
         * this is where you would put the code to do it.
         */

        context.close();
    }
}
