package org.nijhazer.selenium;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

public class DriverServiceBuilder {
    private static Logger logger = LoggerFactory.getLogger(DriverServiceBuilder.class);
    private File chromeDriverExecutable;

    public ChromeDriverService getChrome() {
        return new ChromeDriverService.Builder()
                .usingDriverExecutable(chromeDriverExecutable)
                .usingAnyFreePort()
                .build();
    }

    public void setChromeDriverExecutable(Resource chromeDriverResource) throws IOException {
        File file = getDriverFile(chromeDriverResource);
        if (file != null) {
            chromeDriverExecutable = file;
        }
    }

    private File getDriverFile(Resource resource) throws IOException {
        logger.info("Looking for driver file '{}'", resource.getFilename());
        File file = new File("./" + resource.getFilename());
        if (file.exists()) {
            logger.info("Driver file already exists; not updating file");
            return null;
        }

        logger.info("Copying driver from classpath to executing directory");
        FileUtils.copyURLToFile(resource.getURL(), file);

        if (!file.canExecute()) {
            logger.info("Setting driver as executable");
            file.setExecutable(true);
        }

        return file;
    }

    public void close() {
        logger.debug("Closing driver service builder");
        if (chromeDriverExecutable != null) {
            if (chromeDriverExecutable.exists()) {
                logger.debug("Removing driver file '{}'", chromeDriverExecutable.getName());
                chromeDriverExecutable.delete();
            }
        }
    }
}
