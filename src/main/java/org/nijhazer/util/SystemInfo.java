package org.nijhazer.util;

public class SystemInfo {
    public static final String OS;

    private static final String WINDOWS = "windows";
    private static final String MAC = "mac";
    private static final String LINUX = "linux";
    private static final String UNSUPPORTED_OS = "unsupported.os";

    static {
        if (isWindows()) {
            OS = WINDOWS;
        } else if (isMac()) {
            OS = MAC;
        } else if (isUnix()) {
            OS = LINUX;
        } else {
            OS = UNSUPPORTED_OS;
        }
    }

    private static boolean isWindows() {
        return (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0);
    }

    private static boolean isMac() {
        return (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);
    }

    private static boolean isUnix() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0);
    }

    public static boolean isSolaris() {
        return (System.getProperty("os.name").toLowerCase().indexOf("sunos") >= 0);
    }
}
